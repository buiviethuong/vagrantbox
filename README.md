# How to create an your own vagrant box

## I. Install box and neccessary package

### 1. Install vagrant and virtual box

Vagrant: https://www.vagrantup.com/downloads.html <br>
Virtual box: https://www.virtualbox.org/wiki/Downloads
### 2. Install vagrant box and neccessary software
    vagrant init hashicorp/precise64; //I use this box, you can use another.
    vagrant up
    vagrant ssh
    sudo apt-get update
    sudo apt-get upgrade
    sudo apt-get install vim
    sudo apt-get install apache2
    sudo apt-get install mysql-server libapache2-mod-auth-mysql php5-mysql
    sudo mysql_install_db
    sudo /usr/bin/mysql_secure_installation
    sudo apt-get install php5 libapache2-mod-php5 php5-mcrypt
    sudo apt-get install php5-cgi php5-cli php5-curl php5-common php5-gd php5-mysql
    sudo service apache2 restart

### 3. Make the box smaller
    sudo apt-get clean
    sudo dd if=/dev/zero of=/EMPTY bs=1M
    sudo rm -f /EMPTY
    cat /dev/null > ~/.bash_history && history -c && exit

### 4. Export box
    vagrant package --output vagrant.box

### 5. Add new box again
    vagrant box add mybox vagrant.box

### 6. Remove old box and init new box
    vagrant destroy
    rm Vagrantfile
    vagrant init mybox
### 7. Configure Vagrantfile
* Remove "#" at ` # config.vm.network "private_network", ip: "192.168.33.10" `
* Remove "#" at ` # config.vm.synced_folder "/src", "/var/www" ` to config sync folder from local to vagrant box.
        `/src` is local source directory want to sync, `/var/www` is apache root. In my case, it was `/var/www`, try `/var/www/html` if your apache have root /var/www/html.
    
## II. Config remote MySQL by MySQL client(MySQL Workbench, Navicat, Sequel Pro,...)
- Open file my.cnf `sudo vi /etc/mysql/my.cnf`

- Comment 2 row
    `# bind-address = 127.0.0.1`
    `# skip-external-locking`
    
- Restart MySQL
    `sudo service mysql restart`
    
- Config remote access for root user
    
    ```
    mysql -u root -p
    GRANT ALL PRIVILEGES ON *.* TO root@‘%’ IDENTIFIED BY ‘yourpassword’;
    FLUSH PRIVILEGES;
    ```

- Connect to MySQL:
host: `192.168.33.10`,
port: `22`,
username/password: `root/root` (I config when install MySQL)
